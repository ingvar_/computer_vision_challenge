#Proekspert Data Science Assignment

Requirements:

+ Python3 and the following packages:
+ numpy
+ matplotlib
+ scipy
+ PIL

The code is presented using Jupyter Notebook (*Proekspert_assignment.ipynb*).

---

Visible spectrum is the portion of electromagnetic spectrum, which contains waves with wavelengths from around 400nm to 700nm. Regular photos are taken by looking at the whole visual spectrum. Multispectral images can be created by looking at finer portions of the spectrum and creating a grayscale image for each spectrum band. By taking multispectral images over the whole visible spectrum (or even broader spectrum) it is possible to get more detailed information about the properties of the object.

Regular images are often stored in RGB format, having three channels - red, green and blue. This means that 512x512 pixel color image can be viewed as a multidimensional array with dimensions 512x512x3, where element value is between 0 and 255. If for example all values in red channel are 255, and all values in other channels are 0, we get red color. If all
values from all channels are 128, we get gray. If all values from all channels are 255, we get white.

My task was to combine images from *multispectral_images* directory into a regular colored image. The resulting image should be similar to *sponges_RGB.bmp* found in root directory. The content of this directory includes full spectral resolution reflectance data from 400nm to 700nm at 10nm steps (31 bands total). Each band is stored as a 512x512 dimensional 16-bit grayscale PNG image. Image filenames are of the format *sponges ms 01.png*, where the ’01’ at the end signifies that this is the first image (captured at 400nm). Thus, ’02’ corresponds to 410nm, and so on, until ’31’ for 700nm.

To solve this task, I used a function (modified from Stackoverflow) to generate RGB weights corresponding to wavelengths. I concatenated all the grayscale image pixel intensities, found the maximum intensity and normalised the data. Then, I multiplied all the pixel intensities by the corresponding R, G and B weights, summed them together and divided by the sum of the weights (weighted arithmetic mean). I combined the weighted means to a single RGB array in uint8 format and converted it to an image again. At the end, there is a comparison between the obtained image and the original image.